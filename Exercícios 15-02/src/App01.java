import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Victor R. F. (2017101820)
 * @version 1.0.0
 */

public class App01 {

	public static void main(String[] args) {
		
		try {
			
			//Declara��o de vari�veis
			
			double AV1, AV2, AV3, media;
			Scanner tecla = new Scanner(System.in);
			
			for (int i=0; i < 5; i++) {
				System.out.println("===== Aluno " + i + "=========");
				//Entrada de dados
			
				System.out.println("Digite sua nota da AV1 ");
				AV1 = tecla.nextDouble();
				System.out.println("Digite sua nota da AV2 ");
				AV2 = tecla.nextDouble();
				System.out.println("Digite sua nota da AV3 ");
				AV3 = tecla.nextDouble();
				
				//Processamento de Dados
				
				media = (AV1+AV2+AV3)/3;
				
				//Sa�da da informa��o
				
				if (media >= 7) {
					System.out.println("Aprovado! Sua m�dia �: " + media);
				}else {
					System.out.println("Reprovado! Sua m�dia �: " + media);
				}
			}
			
			
			
		} catch(InputMismatchException x) {
			
			System.out.println("Digite apenas um n�mero!");
			
		}
		
		catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
	}

}
